const config = require('dotenv').config();
const express = require('express');
require('./api/db/games.db');
const router = require('./api/router/router');

console.log('config', config);

const app = express();
app.use(express.json());
app.use("/api", router);


const server = app.listen(process.env.port, function () {
    console.log('Mean game server started at', server.address().port);
});