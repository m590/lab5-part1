const express = require('express');
const gameCtrl = require('../controller/games.controller');

const router = express.Router();

router.route("/games")
    .get(gameCtrl.gamesGetAll)
    .post(gameCtrl.gamesAddOne);

router.route("/games/:gameId")
    .get(gameCtrl.gamesGetOne)
    .put(gameCtrl.gamesFullUpdateOne)
    .patch(gameCtrl.gamesPartialUpdateOne)
    .delete(gameCtrl.gamesDeleteOne);

module.exports = router;